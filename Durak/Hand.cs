﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Durak
{
    class Hand
    {
        public List<GameCard> HandList = new List<GameCard>();
        static public Random ran = new Random();
        int rand = ran.Next(36);
        public void StartHand(bool isInviz = false)
        {

            while (HandList.Count < 6)
            {
                if (GameCardGenerator.Deck[rand] != null)
                {
                    if(isInviz == true)
                        GameCardGenerator.Deck[rand].IsInviz = true;
                    HandList.Add(GameCardGenerator.Deck[rand]);
                    GameCardGenerator.deckCapasity--;
                }
                GameCardGenerator.Deck[rand] = null;
                rand = ran.Next(36);
            }
            
        }
        public void DeckDrow(bool isInviz = false)
        {
            while (HandList.Count < 6)
            {
                if (GameCardGenerator.deckCapasity <= 0)
                {
                    GameCardGenerator.Deck[1] = GameCardGenerator.Deck[36];
                    GameCardGenerator.Deck[36] = null;
                }
                if (GameCardGenerator.Deck[36] == null)
                    break;


                if (GameCardGenerator.Deck[rand] != null)
                {
                    if (isInviz == true)
                        GameCardGenerator.Deck[rand].IsInviz = true;
                    HandList.Add(GameCardGenerator.Deck[rand]);
                    GameCardGenerator.deckCapasity--;
                }

                GameCardGenerator.Deck[rand] = null;
                rand = ran.Next(36);
            }
        }

        public override string ToString()
        {
            string mystr="";
            for (int i = 0; i < HandList.Count; i++)
            {
                mystr += HandList[i];
            }
            return mystr;
        }

    }
}
