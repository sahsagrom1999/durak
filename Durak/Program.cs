﻿using System;
using System.Threading;


namespace Durak
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(" _____ \n| *** |\n|*    |\n|* ***|\n|*   *|\n| *** |\n|     |\n|  *  |\n| *** |\n|*****|\n|  *  |\n| *** |\n|_____|");
            //Console.Read();

            string comand = "";
            string history = "";
            bool myTurn = true;

            GameCardGenerator.DeckGenerator();
            var myHand = new Hand();
            var myTable = new Table();
            var myOponentHand = new Hand();
            myOponentHand.StartHand(true);
            myHand.StartHand();

            while (comand != "endgame")
            {
                Console.WriteLine($"is it my turn {myTurn}      deck cap {GameCardGenerator.deckCapasity}    debad{myHand.HandList.Count}");
                PrintAll(myHand, myOponentHand, myTable);

                if (myTurn == true)
                {
                    comand = Console.ReadLine();
                    int number = -1;
                    Int32.TryParse(comand, out number);
                    number = number - 1;

                    if (comand == "hang up")
                    {
                        myTurn = false;
                        myTable.TableList.Clear();
                        myOponentHand.DeckDrow(true);
                        myHand.DeckDrow();
                        Console.Clear();
                        continue;
                    }

                    //MyTurn(number, myHand, myTable);
                    if (number > -1 && number < myHand.HandList.Count)
                    {
                        bool isOnTable = false;
                        for (int i = 0; i < myTable.TableList.Count; i++)
                        {
                            if (myHand.HandList[number].Pover == myTable.TableList[i].Pover)
                            {
                                isOnTable = true;
                                break;
                            }
                        }

                        if (myTable.TableList.Count == 0 || isOnTable == true)
                        {
                            myTable.TableList.Add(myHand.HandList[number]);
                            myHand.HandList.Remove(myHand.HandList[number]);
                        }

                    }
                    else
                        continue;

                    if (myTable.TableList.Count != 0)
                    {
                        GameCard goodCard = null;
                        for (int i = 0; i < myOponentHand.HandList.Count; i++)
                        {
                            if (myOponentHand.HandList[i].Pover > myTable.TableList[myTable.TableList.Count - 1].Pover &&
                                myOponentHand.HandList[i].Suit == myTable.TableList[myTable.TableList.Count - 1].Suit)
                            {
                                goodCard = myOponentHand.HandList[i];
                                break;
                            }
                            else if ((myOponentHand.HandList[i].Suit == GameCardGenerator.Deck[36].Suit &&
                                myTable.TableList[myTable.TableList.Count - 1].Suit != GameCardGenerator.Deck[36].Suit))
                                goodCard = myOponentHand.HandList[i];
                        }
                        if (goodCard != null)
                        {
                            myTable.TableList.Add(goodCard);
                            myOponentHand.HandList.Remove(goodCard);
                            Console.Clear();
                            continue;
                        }
                        else
                        {
                            myOponentHand.HandList.AddRange(myTable.TableList);
                            myTable.TableList.Clear();
                            myHand.DeckDrow();
                            Console.Clear();
                            continue;
                        }
                    }

                    history += $"{comand} \n";

                    if (comand == "history")
                    {
                        Console.Clear();
                        Console.WriteLine($"{history}");
                        Console.ReadLine();
                    }

                    Console.Clear();
                }
                else //
                {
                    if (myTable.TableList.Count == 0)
                    {
                        GameCard goodCard = myOponentHand.HandList[0];
                        for (int i = 0; i < myOponentHand.HandList.Count; i++)
                        {
                            if (goodCard.Pover > myOponentHand.HandList[i].Pover &&
                                myOponentHand.HandList[i].Suit != GameCardGenerator.mainSuit)
                            {
                                goodCard = myOponentHand.HandList[i];
                            }
                        }
                        if (goodCard.Suit == GameCardGenerator.mainSuit)
                        {
                            for (int i = 0; i < myOponentHand.HandList.Count; i++)
                            {
                                if (goodCard.Pover < myOponentHand.HandList[i].Pover)
                                {
                                    goodCard = myOponentHand.HandList[i];
                                }
                            }
                        }
                        if (goodCard != null)
                        {
                            myTable.TableList.Add(goodCard);
                            myOponentHand.HandList.Remove(goodCard);
                            Console.Clear();
                            continue;
                        }
                    }
                    else if(myTable.TableList.Count%2 == 0)
                    {
                        GameCard goodCard = null;
                        for (int i = 0; i < myTable.TableList.Count; i++)
                        {
                            for (int j = 0; j < myOponentHand.HandList.Count; j++)
                            {
                                if (myOponentHand.HandList[j].Pover == myTable.TableList[i].Pover)
                                {
                                    goodCard = myOponentHand.HandList[j];
                                    break;
                                }
                            }
                        }
                        if (goodCard !=null)
                        {
                            myTable.TableList.Add(goodCard);
                            myOponentHand.HandList.Remove(goodCard);
                            Console.Clear();
                            continue;
                        }
                        if(goodCard == null)
                        {
                            myTurn = true;
                            myTable.TableList.Clear();
                            myOponentHand.DeckDrow();
                            myHand.DeckDrow();
                            Console.Clear();
                            continue;
                        }

                    }



                    comand = Console.ReadLine();
                    int number = -1;
                    Int32.TryParse(comand, out number);
                    number = number - 1;

                    if (comand == "take")
                    {
                        myTurn = false;
                        myHand.HandList.AddRange(myTable.TableList);
                        myTable.TableList.Clear();
                        

                        myOponentHand.DeckDrow(true);
                        Console.Clear();
                        continue;
                    }

                    if (number > -1 && number < myHand.HandList.Count)
                    {
                        if (myHand.HandList[number].Pover > myTable.TableList[myTable.TableList.Count - 1].Pover &&
                            myHand.HandList[number].Suit == myTable.TableList[myTable.TableList.Count - 1].Suit)
                        {
                            myTable.TableList.Add(myHand.HandList[number]);
                            myHand.HandList.Remove(myHand.HandList[number]);
                            

                        }
                        else if (myHand.HandList[number].Suit == GameCardGenerator.mainSuit &&
                            myHand.HandList[number].Suit != myTable.TableList[myTable.TableList.Count - 1].Suit)
                        {
                            myTable.TableList.Add(myHand.HandList[number]);
                            myHand.HandList.Remove(myHand.HandList[number]);                           
                        }
                    }

                    Console.Clear();

                }


            }
            Console.WriteLine("Thant's for playing");
        }
        static public void PrintAll(Hand mh, Hand moh, Table t)
        {
            Console.WriteLine("MyOponent Hand:");
            PrintHand(moh);

            Console.WriteLine("\n\n");
            Console.WriteLine("Table:");
            PrintTable(t);
            Console.WriteLine("\n\n\n\n");

            Console.WriteLine("MyHand:");
            PrintHand(mh);

            Console.WriteLine("\n\nYour comands: ");
        }
        static public void MyTurn(int numb, Hand mHand, Table mTable)
        {
            if (numb > -1 && numb < mHand.HandList.Count)
            {
                bool isOnTable = false;
                for (int i = 0; i < mTable.TableList.Count; i++)
                {
                    if (mHand.HandList[numb].Pover == mTable.TableList[i].Pover)
                    {
                        isOnTable = true;
                        break;
                    }
                }
                if (mTable.TableList.Count == 0 || isOnTable == true)
                {
                    mTable.TableList.Add(mHand.HandList[numb]);
                    mHand.HandList.Remove(mHand.HandList[numb]);
                }
            }
        }
        static public void PrintHand(Hand hand)
        {
            if (hand.HandList.Count == 0)
                Console.Write($"hand is empty");
            else
            {
                Console.Write($"|");
                for (int i = 0; i < hand.HandList.Count; i++)
                {

                    Console.Write($" {hand.HandList[i]} |");
                }
            }
        }
        static public void PrintTable(Table table)
        {      
            for (int i = 0; i < table.TableList.Count; i++)
            {
                Console.Write($" {table.TableList[i]} ");                
            }
            Console.Write($"\n                                                               Main suit is: {GameCardGenerator.Deck[36]}");
        }
        static public void ItsWork(string str ="")
        {
            Console.Clear();
            Console.WriteLine($"Its Work {str}");
            Console.Read();
        }
    }
}