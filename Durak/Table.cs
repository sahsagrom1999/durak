﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Durak
{
    class Table
    {
        public List<GameCard> TableList = new List<GameCard>();
      
        public override string ToString()
        {
            string mystr = "";
            for (int i = 0; i < TableList.Count; i++)
            {
                mystr += TableList[i];
            }
            return mystr;
        }
    }
}
