﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Durak
{
    class GameCard
    {
        enum Suits
        {
            Diamonds,
            Hearts,
            Spades,//пики
            Clubs
        }
        public string Suit { get; } //масть
        public int Pover { get; }
        public string Number { get; }
        public bool IsTrump { get; set; }
        public bool IsInviz { get; set; }

        public GameCard(string suit, int pover ,  string number)
        {
            Suit = suit;
            Pover = pover;
            Number = number;
        }
        public override string ToString()
        {
            string str;
            string strinv = "*******";
            str = $"{Number} {Suit} ";
            //if (IsInviz == true)
                //return strinv;
            return str;
        }

    }
}
